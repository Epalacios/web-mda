<?php
/**
 * Generate class for main menu icon
 *
 * @package     Wow_Plugin
 * @subpackage  Admin/Main_page
 * @author      Wow-Company <helper@wow-company.com>
 * @copyright   2019 Wow-Company
 * @license     GNU Public License
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

include_once plugin_dir_path( __FILE__ ) . 'settings/options/icons.php';

?>
<div class="about-wrap wow-support">
    <div class="feature-section one-col">
        <div class="col">

            <div class="wow-plugin">

                <h3>
					<?php _e( 'Generate icon class for main menu', 'bubble-menu' ); ?>
                </h3>


                <div class="columns">
                    <div class="column">
                        <div class="field">
                            <label class="label">
	                            <?php _e( 'Select icon', 'bubble-menu' ); ?>
                            </label>
                            <select class="icons" id="select_icon">
		                        <?php
		                        $icon_select = '';
		                        foreach ( $icons as $icon ) {
			                        $icon_select .= '<option value="' . $icon . '">' . $icon . '</option>';
		                        };
		                        echo $icon_select;
		                        ?>
                            </select>
                        </div>

                    </div>
                    <div class="column">
                        <div class="field">
                            <label class="label">
	                            <?php _e( 'Icon position', 'bubble-menu' ); ?>
                            </label>
                            <select id="icon_position">
                                <option value=""><?php _e( 'Before Menu item', 'bubble-menu' ); ?></option>
                                <option value=" fa-after"><?php _e( 'After Menu item', 'bubble-menu' ); ?></option>
                            </select>
                        </div>


                    </div>
                </div>

                <div class="container">
                    <div class="element">
                        <h4><?php _e( 'Icon Class', 'bubble-menu' ); ?>: <span id="icon_class"
                                                                                       style="color:#37c781;"/></span>
                        </h4>
                    </div>
                </div>

                <fieldset class="triggers-open-notice">
                    <legend style="color:red"><?php _e( 'Notice!', 'bubble-menu' ); ?></legend>
                    <div class="container">
                        <div class="element">
							<?php _e( 'You can open popup via adding to the element:', 'bubble-menu' ); ?>
                            <ul>
                                <li>&bull; Copy Icon Class</li>
                                <li>&bull; Go to Appearance -> Menus, select which menu item to which you want to add
                                    the icon, and add the icon class(es) under 'CSS Classes (optional)'. Click 'Screen
                                    Options' (top right of screen) and make sure that 'CSS Classes' is checked. If not -
                                    check it!
                                </li>
                                <li>&bull; Paste Icon Class to item 'CSS Classes'</li>
                            </ul>
                        </div>
                    </div>
                </fieldset>


            </div>
        </div>
    </div>
</div>
