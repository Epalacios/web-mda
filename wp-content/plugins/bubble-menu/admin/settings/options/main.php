<?php
/**
 * Main Settings param
 *
 * @package     Wow_Plugin
 * @copyright   Copyright (c) 2018, Dmytro Lobov
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
 */

include_once( 'icons.php' );
$icons_new = array();
foreach ( $icons as $key => $value ) {
  $icons_new[ $value ] = $value;
}

// Position of the menu
$menu = array(
	'id'     => 'position',
	'name'   => 'param[menu]',
	'type'   => 'select',
	'val'    => isset( $param['menu'] ) ? $param['menu'] : 'left',
	'option' => array(
		'wow-bmp-pos-t'  => __( 'Top (max 9 items)', 'bubble-menu' ),
		'wow-bmp-pos-r'  => __( 'Right (max 9 items)', 'bubble-menu' ),
		'wow-bmp-pos-b'  => __( 'Bottom (max 9 items)', 'bubble-menu' ),
		'wow-bmp-pos-l'  => __( 'Left (max 9 items)', 'bubble-menu' ),
		'wow-bmp-pos-tl' => __( 'Top-left (max 5 items)', 'bubble-menu' ),
		'wow-bmp-pos-tr' => __( 'Top-right (max 5 items)', 'bubble-menu' ),
		'wow-bmp-pos-br' => __( 'Bottom-right (max 5 items)', 'bubble-menu' ),
		'wow-bmp-pos-bl' => __( 'Bottom-left (max 5 items)', 'bubble-menu' ),
	),
);

// Menu position help
$menu_help = array(
	'text' => __( 'Specify menu position on screen.', 'bubble-menu' ),
);

// Button shapes
$shapes = array(
	'name'   => 'param[shapes]',
	'id'     => 'shapes',
	'class'  => '',
	'type'   => 'select',
	'val'    => isset( $param['shapes'] ) ? $param['shapes'] : '',
	'option' => array(
		''                      => __( 'Circle', 'bubble-menu' ),
	),
	'func'   => '',
);


// Button shapes help
$shapes_help = array(
	'text' => __( 'Choose the shape for menu buttons.', 'bubble-menu' ),
);

// Animation
$animation = array(
	'name'   => 'param[animation]',
	'id'     => 'animation',
	'class'  => '',
	'type'   => 'select',
	'val'    => isset( $param['animation'] ) ? $param['animation'] : '',
	'option' => array(
		'' => __( 'One by one', 'bubble-menu' ),
	),
);


// Animation help
$animation_help = array(
	'text' => __( 'Set the animation effect for opening menu.', 'bubble-menu' ),
);

// Icon
$menu_icon = array(
	'name'   => 'param[menu_icon]',
	'class'  => 'icons',
	'type'   => 'select',
	'val'    => isset( $param['menu_icon'] ) ? $param['menu_icon'] : 'fas fa-hand-point-up',
	'option' => $icons_new,
);

// Color
$color = array(
	'name'   => 'param[color]',
	'id'     => 'color',
	'class'  => 'item-bgcolor',
	'type'   => 'select',
	'val'    => isset( $param['color'] ) ? $param['color'] : 'white',
	'option' => array(
		'white'  => __( 'White', 'bubble-menu' ),
	),
	'func'   => '',
);

// Hover color
$hcolor = array(
	'name'   => 'param[hcolor]',
	'id'     => 'hcolor',
	'class'  => 'item-color',
	'type'   => 'select',
	'val'    => isset( $param['hcolor'] ) ? $param['hcolor'] : 'black',
	'option' => array(
		'black'  => __( 'Black', 'bubble-menu' ),
	),
);

$main_id = array(
	'name' => 'param[main_id]',
	'id'   => 'main_id',
	'type' => 'text',
	'val'  => isset( $param['main_id'] ) ? $param['main_id'] : '',
);

$main_id_help = array(
	'text' => __( 'Set ID for element.', 'bubble-menu' ),
);

$main_class = array(
	'name' => 'param[main_class]',
	'id'   => 'main_class',
	'type' => 'text',
	'val'  => isset( $param['main_class'] ) ? $param['main_class'] : '',
);

$main_class_help = array(
	'title' => __( 'Set Class for element.', 'bubble-menu' ),
	'ul'    => array(
		__( 'You may enter several classes separated by a space.', 'bubble-menu' ),
	)
);



$main_alt_help = array(
	'text' => esc_attr__( 'Set the attribute Alt for custom image.', 'bubble-menu' ),
);
