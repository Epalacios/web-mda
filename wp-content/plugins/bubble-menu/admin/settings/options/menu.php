<?php
/**
 * Settings
 *
 * @package     Wow_Plugin
 * @copyright   Copyright (c) 2018, Dmytro Lobov
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
 */


$count_i = ( ! empty( $param['item_type'] ) ) ? count( $param['item_type'] ) : '0';
if ( $count_i > 0 ) {
	for ( $i = 0; $i < $count_i; $i ++ ) {

		// Icon
		$item_icon_[ $i ] = array(
			'name'   => 'param[item_icon][]',
			'class'  => 'icons',
			'type'   => 'select',
			'val'    => isset( $param['item_icon'][ $i ] ) ? $param['item_icon'][ $i ] : 'fas fa-hand-point-up',
			'option' => $icons_new,
		);

		$item_tooltip_include_[ $i ] = array(
			'name'  => 'param[item_tooltip_include][]',
			'type'  => 'checkbox',
			'class' => 'tooltip-include',
			'val'   => isset( $param['item_tooltip_include'][ $i ] ) ? $param['item_tooltip_include'][ $i ] : 0,
			'func'  => 'checkboxchecked(this); itemtooltip(this);',
		);

		// Label for item
		$item_tooltip_[ $i ] = array(
			'name'  => 'param[item_tooltip][]',
			'class' => 'item-tooltip',
			'type'  => 'text',
			'val'   => isset( $param['item_tooltip'][ $i ] ) ? $param['item_tooltip'][ $i ] : '',
		);

		$item_tooltip_show_[ $i ] = array(
			'name'   => 'param[item_tooltip_show][]',
			'class'  => '',
			'type'   => 'select',
			'val'    => isset( $param['item_tooltip_show'][ $i ] ) ? $param['item_tooltip_show'][ $i ] : '',
			'option' => array(
				'1' => __( 'On hover', 'bubble-menu' ),
			),
			'func'   => '',
		);


//    $smooth = isset( $param[ 'menu_1' ][ 'scroll' ][ $i ] ) ? $param[ 'menu_1' ][ 'scroll' ][ $i ] : '';

		// Type of the item
		$item_type_[ $i ] = array(
			'name'   => 'param[item_type][]',
			'type'   => 'select',
			'class'  => 'item-type',
			'val'    => isset( $param['item_type'][ $i ] ) ? $param['item_type'][ $i ] : '',
			'option' => array(
				'link'         => esc_attr__( 'Link', 'bubble-menu' ),
			),
			'func'   => 'itemtype(this);',
		);


		// Link URL
		$item_link_[ $i ] = array(
			'name' => 'param[item_link][]',
			'type' => 'text',
			'val'  => isset( $param['item_link'][ $i ] ) ? $param['item_link'][ $i ] : '',
		);


		$open_link_[ $i ] = array(
			'name'   => 'param[open_link][]',
			'id'     => 'open_link',
			'class'  => '',
			'type'   => 'select',
			'val'    => isset( $param['open_link'][ $i ] ) ? $param['open_link'][ $i ] : '',
			'option' => array(
				'_self'  => esc_attr__( 'Same window', 'bubble-menu' ),
			),
			'func'   => '',
		);

		$item_color_[ $i ] = array(
			'name'   => 'param[item_color][]',
			'class'  => 'item-bgcolor',
			'type'   => 'select',
			'val'    => isset( $param['item_color'][ $i ] ) ? $param['item_color'][ $i ] : 'white',
			'option' => array(
				'white'  => esc_attr__( 'White', 'bubble-menu' ),
			),
			'func'   => '',
		);

		$item_hcolor_[ $i ] = array(
			'name'   => 'param[item_hcolor][]',
			'class'  => 'item-color',
			'type'   => 'select',
			'val'    => isset( $param['item_hcolor'][ $i ] ) ? $param['item_hcolor'][ $i ] : 'black',
			'option' => array(
				'black'  => esc_attr__( 'Black', 'bubble-menu' ),
			),
			'func'   => '',
		);

		$button_id_[ $i ] = array(
			'name' => 'param[button_id][]',
			'type' => 'text',
			'val'  => isset( $param['button_id'][ $i ] ) ? $param['button_id'][ $i ] : '',
		);

		$button_class_[ $i ] = array(
			'name' => 'param[button_class][]',
			'type' => 'text',
			'val'  => isset( $param['button_class'][ $i ] ) ? $param['button_class'][ $i ] : '',
		);

		$link_rel_[ $i ] = array(
			'name' => 'param[link_rel][]',
			'type' => 'text',
			'val'  => isset( $param['link_rel'][ $i ] ) ? $param['link_rel'][ $i ] : '',
		);

	}

}


$item_icon_help = array(
	'title' => __( 'Set the icon for menu item. If you want use the custom item:', 'bubble-menu' ),
	'ul'    => array(
		esc_attr__( '1. Check the box on "custom"', 'bubble-menu' ),
		esc_attr__( '2. Upload the icon in Media Library', 'bubble-menu' ),
		esc_attr__( '3. Copy the URL to icon', 'bubble-menu' ),
		esc_attr__( '4. Paste the icon URL to field', 'bubble-menu' ),
	),
);

$item_tooltip_help = array(
	'text' => esc_attr__( 'Set the text for menu item.', 'bubble-menu' ),
);

$item_type_help = array(
	'text' => esc_attr__( 'Select the type of menu item. Explanation of some types:', 'bubble-menu' ),
	'ul'   => array(
		esc_attr__( '<strong>Smooth Scroll</strong> - Smooth scrolling of the page to the specified anchors on the page.',
			'bubble-menu' ),
		esc_attr__( '<strong>ShiftNav Menu</strong> - open the menu, wich create via the plugin ShiftNav.',
			'bubble-menu' ),
	),
);

$hold_open_help = array(
	'text' => esc_attr__( 'When the page loads, the menu item will open.', 'bubble-menu' ),
);

$button_id_help = array(
	'text' => esc_attr__( 'Set the attribute ID for the menu item or left empty.', 'bubble-menu' ),
);

$button_class_help = array(
	'text' => esc_attr__( 'Set the attribute CLASS for the menu item or left empty.', 'bubble-menu' ),
);

$image_alt_help = array(
	'text' => esc_attr__( 'Set the attribute Alt for custom image.', 'bubble-menu' ),
);