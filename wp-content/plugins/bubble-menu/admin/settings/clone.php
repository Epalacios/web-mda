<?php
/**
 * Elements for clone
 *
 * @package     Wow_Plugin
 * @copyright   Copyright (c) 2018, Dmytro Lobov
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

include_once( 'options/clone.php' );
?>


<div class="panel">

    <div class="panel-heading">
        <div class="level-item icon-select" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);">
            <i class="fas fa-hand-point-up"></i>
        </div>
        <div class="level-item">
            <span class="item-label-text">(<?php esc_attr_e( 'no label', 'bubble-menu' ); ?>)</span>
        </div>
        <div class="level-item element-type"></div>
        <div class="level-item toogle-element">
            <span class="dashicons dashicons-arrow-down"></span>
            <span class="dashicons dashicons-arrow-up is-hidden"></span>
        </div>
    </div>

    <div class="toogle-content is-hidden">

        <div class="panel-block">
            <div class="field">
                <label class="label is-small">
					<?php esc_attr_e( 'Label Text', 'bubble-menu' ); ?>
                </label>
				<?php self::option($menu_1_item_tooltip ); ?>
            </div>
        </div>

        <label class="panel-block">
			<?php self::option( $menu_1_item_tooltip_include ); ?><?php esc_attr_e( 'Enable Tooltip', 'bubble-menu' ); ?>
        </label>
        <div class="panel-block">
            <div class="field item-link-blank">
                <label class="label">
					<?php esc_attr_e( 'Show Tooltip', 'bubble-menu' ); ?>
                </label>
				<?php self::option( $menu_1_item_tooltip_show ); ?>
            </div>
        </div>

        <p class="panel-tabs">
            <a class="is-active" data-tab="1"><?php esc_attr_e( "Type", 'bubble-menu' ); ?></a>
            <a data-tab="2"><?php esc_attr_e( "Icon", 'bubble-menu' ); ?></a>
            <a data-tab="3"><?php esc_attr_e( "Style", 'bubble-menu' ); ?></a>
            <a data-tab="4"><?php esc_attr_e( "Attributes", 'bubble-menu' ); ?></a>
        </p>

        <div data-tab-content="1" class="tabs-content">
            <div class="panel-block">
                <div class="field">
                    <label class="label">
						<?php esc_attr_e( 'Item type', 'bubble-menu' ); ?>

                    </label>
					<?php self::option( $menu_1_item_type ); ?>
                </div>
                <div class="field item-link">
                    <label class="label item-link-text">
						<?php esc_attr_e( 'Link', 'bubble-menu' ); ?>
                    </label>
					<?php self::option( $menu_1_item_link ); ?>
                </div>

                <div class="field item-link-blank">
                    <label class="label">
						<?php esc_attr_e( 'Open link', 'bubble-menu' ); ?>
                    </label>
					<?php self::option( $menu_1_open_link ); ?>
                </div>

            </div>

        </div>

        <div data-tab-content="2" class="tabs-content is-hidden">
            <div class="panel-block icon-default">
                <div class="field">
                    <label class="label">
						<?php esc_attr_e( 'Icon', 'bubble-menu' ); ?>
                    </label>
					<?php self::option( $menu_1_item_icon ); ?>
                </div>
            </div>

        </div>

        <div data-tab-content="3" class="tabs-content is-hidden">
            <div class="panel-block">
                <div class="field">
                    <label class="label">
						<?php esc_attr_e( 'Color', 'bubble-menu' ); ?>
                    </label>
					<?php self::option( $menu_1_item_color ); ?>
                </div>
            </div>
            <div class="panel-block">
                <div class="field">
                    <label class="label">
						<?php esc_attr_e( 'Hover Color', 'bubble-menu' ); ?>
                    </label>
					<?php self::option( $menu_1_item_hcolor ); ?>
                </div>
            </div>


        </div>

        <div data-tab-content="4" class="tabs-content is-hidden">
            <div class="panel-block">
                <div class="field">
                    <label class="label">
						<?php esc_attr_e( 'ID for element', 'bubble-menu' ); ?><?php self::tooltip( $button_id_help ); ?>
                    </label>
					<?php self::option( $menu_1_button_id ); ?>
                </div>
            </div>
            <div class="panel-block">
                <div class="field">
                    <label class="label">
						<?php esc_attr_e( 'Class for element', 'bubble-menu' ); ?><?php self::tooltip( $button_class_help ); ?>
                    </label>
					<?php self::option( $menu_1_button_class ); ?>
                </div>
            </div>
            <div class="panel-block">
                <div class="field">
                    <label class="label">
						<?php esc_attr_e( 'Attribute: rel', 'bubble-menu' ); ?>
                    </label>
					<?php self::option( $menu_1_link_rel ); ?>
                </div>
            </div>
        </div>

        <div class="panel-block actions">
            <a class="item-delete"><?php esc_attr_e( "Remove", 'bubble-menu' ); ?></a>
        </div>

    </div>

</div>
