<?php
/**
 * Main Settings
 *
 * @package     Wow_Plugin
 * @copyright   Copyright (c) 2018, Dmytro Lobov
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
include_once( 'options/main.php' );

?>

<div class="panel">
    <div class="panel-heading">
        <div class="level-item icon-select"></div>
        <div class="level-item">
            <span class="item-label-text">
            </span>
        </div>
        <div class="level-item"></div>
    </div>
    <div class="toogle-content">
        <p class="panel-tabs">
            <a class="is-active" data-tab="1"><?php esc_attr_e( "Settings", 'bubble-menu' ); ?></a>
            <a data-tab="2"><?php esc_attr_e( "Icons", 'bubble-menu' ); ?></a>
            <a data-tab="3"><?php esc_attr_e( "Style", 'bubble-menu' ); ?></a>
            <a data-tab="4"><?php esc_attr_e( "Attributes", 'bubble-menu' ); ?></a>
        </p>
        <div data-tab-content="1" class="tabs-content">
            <div class="panel-block">
                <div class="columns is-multiline">
                    <div class="field column is-4">
                        <label class="label">
			                <?php esc_attr_e( 'Position', 'bubble-menu' ); ?><?php echo self::tooltip( $menu_help ); ?>
                        </label>
		                <?php self::option( $menu ); ?>
                    </div>
                    <div class="field column is-4">
                        <label class="label">
			                <?php esc_attr_e( 'Button shapes', 'bubble-menu' ); ?><?php echo self::tooltip( $shapes_help ); ?>
                        </label>
		                <?php self::option( $shapes ); ?>
                    </div>
                    <div class="field column is-4">
                        <label class="label">
			                <?php esc_attr_e( 'Animation', 'bubble-menu' ); ?><?php echo self::tooltip( $animation_help ); ?>
                        </label>
		                <?php self::option( $animation ); ?>
                    </div>
                </div>
            </div>
        </div>
        <div data-tab-content="2" class="tabs-content is-hidden">
            <div class="panel-block icon-default">
                <div class="field">
                    <label class="label">
				        <?php esc_attr_e( 'Icon', 'bubble-menu' ); ?>
                    </label>
			        <?php self::option( $menu_icon ); ?>
                </div>
            </div>

        </div>
        <div data-tab-content="3" class="tabs-content is-hidden">
            <div class="panel-block">
                <div class="field">
                    <label class="label">
				        <?php esc_attr_e( 'Color', 'bubble-menu' ); ?>
                    </label>
			        <?php self::option( $color ); ?>
                </div>
            </div>
            <div class="panel-block">
                <div class="field">
                    <label class="label">
				        <?php esc_attr_e( 'Hover color', 'bubble-menu' ); ?>
                    </label>
			        <?php self::option( $hcolor ); ?>
                </div>
            </div>
        </div>
        <div data-tab-content="4" class="tabs-content is-hidden">
            <div class="panel-block">
                <div class="field">
                    <label class="label">
				        <?php esc_attr_e( 'ID for element', 'bubble-menu' ); ?><?php self::tooltip( $main_id_help ); ?>
                    </label>
			        <?php self::option( $main_id ); ?>
                </div>
            </div>
            <div class="panel-block">
                <div class="field">
                    <label class="label">
					    <?php esc_attr_e( 'Class for element', 'bubble-menu' ); ?><?php self::tooltip( $main_class_help ); ?>
                    </label>
				    <?php self::option( $main_class ); ?>
                </div>
            </div>

        </div>

    </div>
</div>