<?php
/**
 * Public Class
 *
 * @package     Wow_Plugin
 * @subpackage  Public
 * @author      Wow-Company <helper@wow-company.com>
 * @copyright   2019 Wow-Company
 * @license     GNU Public License
 * @version     1.0
 */

namespace bubble_menu_lite;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class Wow_Plugin_Public
 *
 * @package wow_plugin
 *
 * @property array plugin   - base information about the plugin
 * @property array url      - home, pro and other URL for plugin
 * @property array rating   - website and link for rating
 * @property string basedir - filesystem directory path for the plugin
 * @property string baseurl - URL directory path for the plugin
 */
class Wow_Plugin_Public {

	/**
	 * Setup to frontend of the plugin
	 *
	 * @param array $info general information about the plugin
	 *
	 * @since 1.0
	 */

	public function __construct( $info ) {

		$this->plugin = $info['plugin'];
		$this->url    = $info['url'];
		$this->rating = $info['rating'];

		// Add plugin style in header
		add_shortcode( $this->plugin['shortcode'], array( $this, 'shortcode' ) );

		// old shortcode
		add_shortcode( 'Bubble-Menu-Pro', array( $this, 'shortcode' ) );

		// Display on the site
		add_action( 'wp_footer', array( $this, 'display' ) );

		// icons in menu
		add_filter( 'nav_menu_css_class', array( $this, 'nav_menu_css_class' ) );
		add_filter( 'walker_nav_menu_start_el', array( $this, 'walker_nav_menu_start_el' ), 10, 4 );

	}


	/**
	 * Display a shortcode
	 *
	 * @param $atts
	 *
	 * @return false|string
	 */
	public function shortcode( $atts ) {
		extract( shortcode_atts( array( 'id' => "" ), $atts ) );
		$id = absint( $atts['id'] );

		global $wpdb;
		$table  = $wpdb->prefix . 'wow_' . $this->plugin['prefix'];
		$sSQL   = $wpdb->prepare( "select * from $table WHERE id = %d", $id );
		$result = $wpdb->get_results( $sSQL, 'OBJECT_K' );

		if ( empty( $result ) ) {
			return false;
		}

		$param = unserialize( $result[ $id ]->param );
		$check = $this->check( $param, $id );

		if ( $check === false ) {
			return false;
		}

		$menu = '';
		include( 'partials/public.php' );

		$this->include_style_script( $param, $id );

		return $menu;
	}

	private function include_style_script( $param, $id ) {
		$slug    = $this->plugin['slug'];
		$version = $this->plugin['version'];

		if ( empty( $param['disable_fontawesome'] ) ) {
			$url_icons = $this->plugin['url'] . 'vendors/fontawesome/css/fontawesome-all.min.css';
			wp_enqueue_style( $slug . '-fontawesome', $url_icons, null, '5.15.3' );
		}

		$pre_suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		$url_style = plugin_dir_url( __FILE__ ) . 'assets/css/style' . $pre_suffix . '.css';
		wp_enqueue_style( $slug, $url_style, null, $version );

		$inline_style = $this->style( $param, $id );
		wp_add_inline_style( $slug, $inline_style );
	}

	/**
	 * Create Inline style for elements
	 */
	public function style( $param, $id ) {
		$css = '';
		require 'generator-style.php';

		return $css;

	}

	/**
	 * Display the Item on the specific pages, not via the Shortcode
	 */
	public function go_shortcode( $id ) {
		echo do_shortcode( '[' . esc_attr( $this->plugin['shortcode'] ) . ' id=' . absint( $id ) . ']' );
	}


	/**
	 * Display the Item on the specific pages, not via the Shortcode
	 */
	public function display() {
		global $wpdb;
		$table  = $wpdb->prefix . "wow_" . $this->plugin['prefix'];
		$result = $wpdb->get_results( "SELECT * FROM " . $table . " order by id asc" );
		if ( count( $result ) < 1 ) {
			return false;
		}

		foreach ( $result as $key => $val ) {
			$param   = unserialize( $val->param );
			$display = ! empty( $param['show'] ) ? $param['show'] : 'all';
			$id      = $val->id;
			switch ( $display ) {
				case 'all':
					$this->go_shortcode( $id );
					break;
			}

		}

	}

	private function check_status( $param ) {
		if ( ! empty( $param['status'] ) ) {
			return false;
		}

		return true;
	}

	private function check_test_mode( $param ) {
		if ( ! empty( $param['test_mode'] ) && ! current_user_can( 'administrator' ) ) {
			return false;
		}

		return true;
	}

	private function check( $param, $id ) {
		$check     = true;
		$check_arr = array(
			'status'    => $this->check_status( $param ),
			'test_mode' => $this->check_test_mode( $param ),
		);

		foreach ( $check_arr as $value ) {
			if ( $value === false ) {
				$check = false;
				break;
			}
		}

		return $check;
	}


	// Icons in menu

	function nav_menu_css_class( $classes ) {
		if ( is_array( $classes ) ) {
			$tmp_classes = preg_grep( '/^(fa)(-\S+)?$/i', $classes );
			if ( ! empty( $tmp_classes ) ) {
				$classes = array_values( array_diff( $classes, $tmp_classes ) );
				$url_icons = $this->plugin['url'] . 'vendors/fontawesome/css/fontawesome-all.min.css';
				wp_enqueue_style( $this->plugin['slug'] . '-fontawesome', $url_icons, null, '5.15.3' );
			}
		}

		return $classes;
	}


	function walker_nav_menu_start_el( $item_output, $item, $depth, $args ) {
		if ( is_array( $item->classes ) ) {
			$classes = preg_grep( '/^(fa)(-\S+)?$/i', $item->classes );
			if ( ! empty( $classes ) ) {
				$item_output = self::replace_item( $item_output, $classes );

			}
		}

		return $item_output;
	}

	function replace_item( $item_output, $classes ) {


		if ( ! in_array( 'fa', $classes ) ) {
			array_unshift( $classes, 'fa' );
		}

		$before = true;
		if ( in_array( 'fa-after', $classes ) ) {
			$classes = array_values( array_diff( $classes, array( 'fa-after' ) ) );
			$before  = false;
		}

		$icon = '<span class="' . implode( ' ', $classes ) . ' "></span>';

		preg_match( '/(<a.+>)(.+)(<\/a>)/i', $item_output, $matches );
		if ( 4 === count( $matches ) ) {
			$item_output = $matches[1];
			if ( $before ) {
				$item_output .= $icon . ' <span class="wow-bmp-text">' . $matches[2] . '</span>';
			} else {
				$item_output .= '<span class="wow-bmp-text">' . $matches[2] . '</span> ' . $icon;
			}
			$item_output .= $matches[3];
		}

		return $item_output;
	}
}
